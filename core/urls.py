from django.urls import path, include
from rooms import views as room_views

# 바로 아래의 app_name 과 config 폴더 내에 있는 urls.py 의 namespace 가 서로 같은 "core" 여야만 한다.
app_name = "core"

urlpatterns = [
    path("", room_views.HomeView.as_view(), name="home"),
    
]