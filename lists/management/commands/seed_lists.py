import random
from django.core.management.base import BaseCommand
from django.contrib.admin.utils import flatten
from django_seed import Seed
from lists import models as list_models
from users import models as user_models
from rooms import models as room_models

NAME = "lists"

class Command(BaseCommand):

    help = f"this is make fake {NAME}"

    def add_arguments(self, parser):
        parser.add_argument("--number", default=2, type=int, help=f"얼마나 많은 {NAME} 를 생성하길 희망하십니까?")

    def handle(self, *args, **options):
        number = options.get("number")
        seeder = Seed.seeder()
        
        users = user_models.User.objects.all()
        rooms = room_models.Room.objects.all()
        
        # 이 부분을 해주지 않으면 에러가 발생한다. 
        seeder.add_entity(
            list_models.List, 
            number, 
            {
                
                "user": lambda x: random.choice(users),
            })       
                
    
    
        # 실행을 위한 seeder.execute()
        created = seeder.execute()
        cleaned = flatten(list(created.values()))
        for pk in cleaned:
            list_model = list_models.List.objects.get(pk=pk)
            to_add = rooms[random.randint(0, 5) : random.randint(6, 30)]
            list_model.rooms.add(*to_add)

        self.stdout.write(self.style.SUCCESS(f"{number} {NAME} created!"))


        # times = options.get("times")
        # for t in range(0, int(times)):
        #     print("I will definitely marry Alura.")