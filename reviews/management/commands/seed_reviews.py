import random
from django.core.management.base import BaseCommand
from django.contrib.admin.utils import flatten
from django_seed import Seed
from reviews import models as review_models
from users import models as user_models
from rooms import models as room_models

class Command(BaseCommand):

    help = "this is make fake reviews"

    def add_arguments(self, parser):
        parser.add_argument("--number", default=2, type=int, help="얼마나 많은 review를 생성하길 희망하십니까?")

    def handle(self, *args, **options):
        number = options.get("number")
        seeder = Seed.seeder()
        users = user_models.User.objects.all()
        rooms = room_models.Room.objects.all()
        
        # 이 부분을 해주지 않으면 에러가 발생한다. 
        seeder.add_entity(
            review_models.Review, 
            number, 
            {
                "review": lambda x: seeder.faker.sentence(),
                "accuracy": lambda x: random.randint(0, 6),
                "communication": lambda x: random.randint(0, 6),
                "cleanLiness": lambda x: random.randint(0, 6),
                "location": lambda x: random.randint(0, 6),
                "check_in": lambda x: random.randint(0, 6),
                "value": lambda x: random.randint(0, 6),
                
                "room": lambda x: random.choice(rooms),
                "user": lambda x: random.choice(users),
        })       
                
        # 실행을 위한 seeder.execute()
        seeder.execute()        
        self.stdout.write(self.style.SUCCESS(f"{number} Reviews created!"))


        # times = options.get("times")
        # for t in range(0, int(times)):
        #     print("I will definitely marry Alura.")