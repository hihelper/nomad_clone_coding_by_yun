from django.core.management.base import BaseCommand
from django_seed import Seed
from rooms import models as room_models
from users import models as user_models


class Command(BaseCommand):

    help = "this is command is create users"

    def add_arguments(self, parser):
        parser.add_argument("--number", default=2, type=int, help="얼마나 많은 user를 생성하길 희망하십니까.", 
        )

    def handle(self, *args, **options):
        number = options.get("number")
        seeder = Seed.seeder()
        
        # user를 생성할때 스태프권한과 슈퍼유저를 생성하지 않기 위한 조건
        seeder.add_entity(user_models.User, number, {
            'is_staff':False,
            'is_superuser':False,
        })

        # 실행을 위한 seeder.execute()
        seeder.execute()
        self.stdout.write(self.style.SUCCESS("users created!"))


        # times = options.get("times")
        # for t in range(0, int(times)):
        #     print("I will definitely marry Alura.")