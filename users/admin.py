from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from . import models


@admin.register(models.User)
class CustomUserAdmin(UserAdmin):
    
    """ Custom User Admin """

    fieldsets = UserAdmin.fieldsets + (("banana 이 부분은 우리가 직접 추가한 부분입니다. 나머지 부분은 장고에서 제공하는 UserAdmin.fieldsets 입니다.",
            {
                "fields": (
                    "avatar",
                    "gender",
                    "job",
                    "birthdate",
                    "language",
                    "currency",
                    "superhost",
                )
            },
        ),
    )
    list_display = (
        "username",
        "first_name",
        "last_name",
        "email",
        "is_active",
        "language",
        "currency",
        "superhost",
        "is_staff",
        "is_superuser",
    )

    
    # 아래와 같은 방식으로 UserAdmin.list_filter 기존의 UserAdmin에 있는 list_filter를 그대로 사용할 수 있다.
    # UserAdmin은 class CustomUserAdmin(UserAdmin):에서 상속받은 django 기본클래스 이다. 
    # 바로 위에 있는 list_display는 아래와같은 방식으로 하지 않았는데. 이렇게 그냥 list_display를 사용하면 대체해버리게 된다. 따라서
    # 기존에 UserAdmin에 있는 list_display는 효력을 상실하고 여기에 있는 list_display로 대체되어 버린다. 두가지 방법의 차이점을 분명히 알아두자!! ^^
    # list_display 를 그냥 이곳에 지정하는 방식과 list_filter = UserAdmin.list_filter + ("superhost",) 이렇게 합쳐서 사용하는 방식 두개 나눠서 꼭 이해하기
    list_filter = UserAdmin.list_filter + ("superhost",)


# admin.site.register(models.User, UserAdmin) 이렇게 작성하는 것과 아래와 같이 작성하는 것은 완전히 동일하다.
# @admin.register(models.User)
# class CustomUserAdmin(UserAdmin):


