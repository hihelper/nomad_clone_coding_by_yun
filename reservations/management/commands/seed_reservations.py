import random
from datetime import datetime, timedelta
from django.core.management.base import BaseCommand
from django_seed import Seed
from reservations import models as reservation_models
from users import models as user_models
from rooms import models as room_models

NAME = "reservations"

class Command(BaseCommand):

    help = f"this is make fake {NAME}"

    def add_arguments(self, parser):
        parser.add_argument("--number", default=2, type=int, help=f"얼마나 많은 {NAME} 를 생성하길 희망하십니까?")

    def handle(self, *args, **options):
        number = options.get("number")
        seeder = Seed.seeder()        
        users = user_models.User.objects.all()
        rooms = room_models.Room.objects.all()
        
        # 이 부분을 해주지 않으면 에러가 발생한다. 
        seeder.add_entity(reservation_models.Reservation, number, 
        {
        "status": lambda x: random.choice(["pending","confirmed","canceled",]),
        "room": lambda x: random.choice(rooms),
        "guest": lambda x: random.choice(users),     
        "check_in": lambda x: datetime.now() - timedelta(days=random.randint(0, 10)),
        "check_out": lambda x: datetime.now() + timedelta(days=random.randint(3, 25)),
        }) 

        seeder.execute()
        self.stdout.write(self.style.SUCCESS(f"{number} {NAME} created!"))


        # times = options.get("times")
        # for t in range(0, int(times)):
        #     print("I will definitely marry Alura.")