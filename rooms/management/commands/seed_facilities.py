from django.core.management.base import BaseCommand
from rooms.models import Facility


class Command(BaseCommand):

    help = "this is help"

    # def add_arguments(self, parser):
    #     parser.add_argument(
    #         "--times", 
    #         help="this is help"
    #     )

    def handle(self, *args, **options):
        facilities = [
            '개인입구',
            '엘리베이터',
            '헬스장',
            '유료주차장',
            '무료주차장',
        ]
        
        for a in facilities:
            Facility.objects.create(name=a)
        self.stdout.write(self.style.SUCCESS("Facility created!"))


        # times = options.get("times")
        # for t in range(0, int(times)):
        #     print("I will definitely marry Alura.")