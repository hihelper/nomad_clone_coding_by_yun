import random
from django.core.management.base import BaseCommand
from django.contrib.admin.utils import flatten
from django_seed import Seed
from rooms import models as room_models
from users import models as user_models


class Command(BaseCommand):

    help = "this is make fake rooms"

    def add_arguments(self, parser):
        parser.add_argument("--number", default=2, type=int, help="얼마나 많은 room을 생성하길 희망하십니까?")

    def handle(self, *args, **options):
        number = options.get("number")
        seeder = Seed.seeder()
        all_users = user_models.User.objects.all()
        room_types = room_models.RoomType.objects.all()
        
        # 이 부분을 해주지 않으면 에러가 발생한다. 
        seeder.add_entity(room_models.Room, number, 
        {
         "name": lambda x: seeder.faker.address(),
         "host": lambda x: random.choice(all_users),
         "room_type": lambda x: random.choice(room_types),
         "price": lambda x: random.randint(0, 15000),
         "beds": lambda x: random.randint(1, 5),
         "bedrooms": lambda x: random.randint(1, 5),
         "baths": lambda x: random.randint(1, 5),
         "guests": lambda x: random.randint(1, 5),         
        })       
                # 실행을 위한 seeder.execute()
        
        created_photos = seeder.execute()
        # created_clean은 새로 생성된 방의 pk값이다.
        created_clean = flatten(list(created_photos.values())) # flatten 를 처음으로 배웠는데 flatten는 아래설명 참조.
        # 백문이 불여일타 아래 코드가 바로 'flatten'다 즉 [[1, 2, 3], [3, 4, 5, 6, 7, 8, 9]]이런 2중 리스트를 [1, 2, 3, 3, 4, 5, 6, 7, 8, 9] 이렇게 만들어 주는 효력이 있다.
        # from django.contrib.admin.utils import flatten
        # n = [[1, 2, 3], [3, 4, 5, 6, 7, 8, 9]]
        # n = flatten(n)

        amenities = room_models.Amenity.objects.all()
        facilities = room_models.Facility.objects.all()
        rules = room_models.HouseRule.objects.all()
        
        for pk in created_clean:
            room = room_models.Room.objects.get(pk=pk)
            for i in range(3, random.randint(10, 30)):
                room_models.Photo.objects.create(
                    caption=seeder.faker.sentence(),
                    room=room,
                    file=f"room_photos/{random.randint(1, 31)}.webp",
                )

            for a in amenities:
                magic_number = random.randint(0, 15)
                if magic_number % 2 == 0:
                    room.amenities.add(a)
            
            for f in facilities:
                magic_number = random.randint(0, 15)
                if magic_number % 2 == 0:
                    room.facilities.add(f)
            
            for r in rules:
                magic_number = random.randint(0, 15)
                if magic_number % 2 == 0:
                    room.rules.add(r)
            
                


        self.stdout.write(self.style.SUCCESS(f"{number} Room created!"))


        # times = options.get("times")
        # for t in range(0, int(times)):
        #     print("I will definitely marry Alura.")