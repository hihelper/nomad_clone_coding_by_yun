from django.core.management.base import BaseCommand
from rooms.models import Amenity


class Command(BaseCommand):

    help = "this is help"

    # def add_arguments(self, parser):
    #     parser.add_argument(
    #         "--times", 
    #         help="this is help"
    #     )

    def handle(self, *args, **options):
        amenities = [
            'tv',
            'toilet',
            'swim_pool',
            'sofa',
            'shower',
            'shopping mall',
        ]
        
        for a in amenities:
            Amenity.objects.create(name=a)
        self.stdout.write(self.style.SUCCESS("Amenities created!"))


        # times = options.get("times")
        # for t in range(0, int(times)):
        #     print("I will definitely marry Alura.")