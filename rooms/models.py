from django.db import models
from django.urls import reverse
from core import models as core_models
from django_countries.fields import CountryField

class AbstractItem(core_models.TimeStampedModel):
    """ Abstract Item """
    
    name = models.CharField(max_length=80)
    
    class Meta:
        abstract = True

    def __str__(self):
        return self.name

class RoomType(AbstractItem):
    """ RoomType Model Definition """
    class Meta:
        verbose_name = "Room Type"
        ordering = ['created']
        # ordering = ['-created']
        # ordering = ['name']

class Amenity(AbstractItem):
    """ Amenity Model Definition """
    class Meta:
        verbose_name_plural = "Amenities"

class Facility(AbstractItem):
    """ Facility Model Definition """
    class Meta:
        verbose_name_plural = "Facilities"

class HouseRule(AbstractItem):
    """ HouseRule Model Definition """
    class Meta:
        verbose_name = "House Rule"

class Photo(core_models.TimeStampedModel):
    """ Photo Model Definition """
    
    caption = models.CharField(max_length=80)
    file = models.ImageField(upload_to="room_photos")
    room = models.ForeignKey('Room', related_name="photos", on_delete=models.CASCADE)
    def __str__(self):
        return self.caption

# Create your models here.
class Room(core_models.TimeStampedModel):
    """ Room Model Definition """

    name = models.CharField(max_length=140)
    description = models.TextField()
    country = CountryField(null=True, blank=True)
    city = models.CharField(max_length=80)
    price = models.IntegerField(null=True, blank=True)
    address = models.CharField(max_length=150)
    guests = models.IntegerField(null=True, blank=True)
    beds = models.IntegerField()
    bedrooms = models.IntegerField()
    baths = models.IntegerField(null=True, blank=True)

    check_in = models.TimeField()
    check_out = models.TimeField()

    instant_book = models.BooleanField(default=False)
    # host 는 ForeignKey로 생성한 뒤 on_delete=models.CASCADE를 해주었다는 점을 반드시 이해해야 합니다.

    host = models.ForeignKey("users.User", related_name="rooms", on_delete=models.CASCADE)
    room_type = models.ForeignKey('RoomType', related_name="rooms", on_delete=models.SET_NULL, null=True)
    # 객실의 유형(room_type)은 방별로 단 1가지만 선택할 수 있어야 하므로 ForeignKey를 선택하였고 객실의 유형을 삭제한다 하더라도 방그자체를 삭제하고 싶진 않기 때문에 on_delete=models.SET_NULL, null=True을 넣어 주었습니다.!!
    amenities = models.ManyToManyField('Amenity', related_name="rooms", blank=True)
    facilities = models.ManyToManyField('Facility', related_name="rooms", blank=True)
    house_rulse = models.ManyToManyField('HouseRule', related_name="rooms", blank=True)


    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.city = str.capitalize(self.city)
        super().save(*args, **kwargs)

    def get_absolute_url(self):
        
        return reverse("rooms:detail", kwargs={"pk": self.pk})
    

    def total_rating(self):
        all_reviews = self.reviews.all()
        all_ratings = 0 
        if len(all_reviews) > 0:
            for review in all_reviews:
                all_ratings += review.rating_average()

            return round(all_ratings / len(all_reviews), 2)
        return 0