from django.contrib import admin
from django.utils.html import mark_safe
from . import models


# admin.py 추가해야 어드민패널에서 볼 수 있고 선택할 수 있습니다.!! 
@admin.register(models.RoomType, models.Facility, models.Amenity, models.HouseRule)
class ItemAdmin(admin.ModelAdmin):

    list_display = (
        "name",
        "used_by",
    )
    def used_by(self, obj):
        return obj.rooms.count()
    pass

class PhotoInline(admin.TabularInline):
    
    model = models.Photo


@admin.register(models.Room)
class RoomAdmin(admin.ModelAdmin):
    
    inlines = (PhotoInline, )

    list_display = (
        'name',
        'country',
        'city',
        'price',
        'guests',
        'beds',
        'bedrooms',
        'baths',
        'check_in',
        'check_out',
        'instant_book',
        'host',
        'count_amenities',
        'count_facilities',
        'count_photos',
        'total_rating',
    )

    ordering = ("name", "price", "bedrooms", )

    list_filter = ('instant_book','host__superhost',"room_type","amenities","facilities","house_rulse",'city','country',)

    raw_id_fields = ("host", )

    search_fields = ("city", "host__username")

    filter_horizontal = ("amenities","facilities","house_rulse",)
    
    def count_amenities(self, obj):
        
        return obj.amenities.count()
    
    def count_facilities(self, obj):
        
        return obj.facilities.count()

    def count_photos(self, obj):
        return obj.photos.count()
    count_photos.short_description = 'Photo Count'

    fieldsets = (
        (
            "Basic Info", {
                "fields": ("name","description","country","city","price","address",)},
        ),
        (
            "Times", {
                "fields": ('check_in','check_out','instant_book',)},
        ),
        (
            "More About The Space", {
                "classes": ("collapse",),
                "fields": ("amenities","facilities","house_rulse",)},
        ),
        (
            "Spaces", {
                "fields": ("guests","beds","bedrooms","baths",)},
        ),
        (
            "Last Details", {
                "fields": ("host",)},
        ),
    )
    

@admin.register(models.Photo)
class PhotoAdmin(admin.ModelAdmin):

    """ Photo Admin Definition """

    list_display = ("__str__", "get_thumbnail", "room")

    def get_thumbnail(self, obj):
        return mark_safe(f'<img src="{obj.file.url}", width="250px" />')

    get_thumbnail.short_description = "Thumbnail"


